import '../company.dart';

Company LoadData(){
  // Have pull from api before loading
  String domain = 'trhhosting.com';
  String name = 'TRH Hosting';

  String base = 'https://${domain}'; //'https://' + domain;
  String contactDomain = 'https://contact.${domain}'; //'https://' + 'contact'+ '.' + domain;
  String license = 'https://${domain}/license'; //'https://' + 'license'+ '.' + domain;
  String blog = 'https://blog.${domain}'; //'https://' + 'blog'+ '.' + domain;
  String sales = 'https://sales.${domain}'; //'https://' + 'blog'+ '.' + domain;
  String help = 'https://help.${domain}'; //'https://' + 'blog'+ '.' + domain;
  Domains domains = new Domains(base, blog, help, sales, contactDomain, license);

  String phone, email;
  phone = '8605551212';
  email = 'buyme@trhhosting.com';
  Contact contact = new Contact(email, phone);

  String sPhone, sEmail;
  sPhone = '8605551213';
  sEmail = 'support@trhhosting.com';

  Support support = new Support(sEmail, sPhone);
  Company x = new Company(name, domains, contact, support);

  return x;
}