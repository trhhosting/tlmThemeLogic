
class Company {
  String name;
  Domains domains;
  Contact contact;
  Support support;
  Company(this.name, this.domains, this.contact, this.support);
}

class Contact {
  String phone;
  String email;
  Contact(this.email, this.phone);
}

class Support {
  String email;
  String phone;
  Support(this.email, this.phone);
}

class Domains {
  String baseDomain;
  String blog;
  String help;
  String sales;
  String contact;
  String license;
  Domains(
      this.baseDomain,
      this.blog,
      this.help,
      this.sales,
      this.contact,
      this.license
  );
}
